const express = require('express');
const app = express();
app.set('secret','asdhadjksahj12');
//允许跨域
app.use(require('cors')());
app.use(express.json());
app.use('/', express.static(__dirname + '/web'))
app.use('/admin', express.static(__dirname + '/admin'))
app.use('/uploads',express.static(__dirname + '/uploads')); //静态资源托管

require('./plugins/db')(app)
require('./plugins/env')(app)
require('./routers/admin')(app)
require('./routers/web')(app)

app.listen(4008,'172.18.85.173',()=>{
  console.log('http://localhost:4008')
});
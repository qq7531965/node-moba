module.exports = app =>{
  const mongoose = require('mongoose');
  mongoose.set('useCreateIndex', true);
  mongoose.connect('mongodb://127.0.0.1:27018/node-moba',{
    useNewUrlParser:true
  });
  //引用模型的所有js 防止关联出错
  require('require-all')(__dirname+ '/../models')
}
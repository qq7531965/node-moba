const mongoose= require('mongoose');
const bcrypt = require('bcryptjs');
const salt = bcrypt.genSaltSync(10);

const schmea = new mongoose.Schema({
    username:{
        type: String,
        required:true,
        index:{
            unique:true
        }
    },
    password:{
        type: String,
        select: false,
        set(val){
            return bcrypt.hashSync(val,salt);
        }   
    },
    isSystemAdmin:{
        type: Boolean,
        required: false,
        default: false
    }
});

module.exports=mongoose.model('AdminUser',schmea);
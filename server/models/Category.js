const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  name:{
    type:String
  },
  parent:{
    // 指定类型为mongoose的objectId 绑定自身 为了查找name的父级
    type:mongoose.SchemaTypes.ObjectId,
    ref:'Category'
  }
});

schema.virtual('children', {
  localField: '_id',
  foreignField: 'parent',
  justOne: false,
  ref: 'Category'
})

schema.virtual('newsList', {
  localField: '_id',
  foreignField: 'categories',
  justOne: false,
  ref: 'Article'
})

module.exports = mongoose.model('Category',schema);
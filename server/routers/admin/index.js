module.exports = app => {
  const express = require('express');
  const router = express.Router({
    mergeParams: true //合并参数
  });
  const assert = require('http-assert')
  const jwt = require('jsonwebtoken')
  const AdminUser = require('../../models/AdminUser')
  //检验中间件
  const authMiddleware = require('../../middleware/auth');
  const resourceMiddleware = require('../../middleware/resource');

  // 新增分类
  router.post('/', async (req, res) => {
    // const model = require(`../../models/${req.params.resuorce}`); //模型通过前端获取
    const model = await req.Model.create(req.body);
    res.send(model);
  });
  // 根据ID修改分类
  router.put('/:id', async (req, res) => {
    // 根据id查找到数据  更新全部信息
    const model = await req.Model.findByIdAndUpdate(req.params.id, req.body);
    res.send(model);
  });
  //获取资源列表
  router.get('/', async (req, res) => {

    const queryOptions = {};
    if (req.Model.modelName === 'Category') {
      queryOptions.populate = 'parent';
    }
    const items = await req.Model.find().setOptions(queryOptions).limit(100);
    res.send(items);
  });

  //  删除分类
  // 根据ID修改分类
  router.delete('/:id', async (req, res) => {
    // 根据id查找到数据  更新全部信息
    await req.Model.findByIdAndDelete(req.params.id, req.body);
    res.send({
      success: true
    });
  });

  //  根据id查找分类
  router.get('/:id', async (req, res) => {
    const model = await req.Model.findById(req.params.id);
    res.send(model);
  });


  // 通用的CRUD
  app.use('/admin/api/rest/:resource',authMiddleware(),resourceMiddleware(),router);

  //处理文件存放路径
  const multer = require('multer');
  const MAO = require('multer-aliyun-oss');
  const upload = multer({
    // dest: __dirname + '/../../uploads'
    storage: MAO({
      config: {
          region: 'oss-cn-shenzhen',
          accessKeyId: 'LTAI4FrrNkfK4YnvVZhjvDtb',
          accessKeySecret: 'uKgNJdFER7THwUV3mLav9iMpLstn37',
          bucket: 'node-moba-hyl'
      }
  })
  })
  //文件上传
  app.post('/admin/api/upload',authMiddleware(),upload.single('file'), async (req, res) => {
    const file = req.file;
    // file.url = `http://localhost:4000/uploads/${file.filename}`;
    res.send(file);
  });

  app.post('/admin/api/login',async (req,res)=>{
    const { username,password } = req.body;
    //1.根据用户名查找用户
    const user = await AdminUser.findOne({username}).select('+password');

    if(!user){
      return res.status(422).send({
        message: '用户不存在！'
      });
    }
    //2.检验密码
    const isValid = require('bcryptjs').compareSync(password,user.password)
    if(!isValid){
      return res.status(422).send({
        message: '密码错误'
      });
    }

    //3.设置token
    const token = jwt.sign({id:user._id},app.get('secret'));
    res.send({token,username});
  });

  //错误处理函数
  app.use(async (err,req,res,next) => {
    res.status(err.statusCode || 500).send({
      message:err.message
    });
  });

}

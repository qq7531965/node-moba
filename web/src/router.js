import Vue from 'vue'
import Router from 'vue-router'
import Main from './views/Main.vue'
import Home from './views/Home.vue'
import Article from './views/Article.vue'
import Hero from './views/Hero.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main,
      children: [
        { path: '/', name: 'home', component: Home },
        { path: '/articles/:id', name: 'articles', component: Article,props:true },
      ]
    },
    {
        path: '/heros/:id', name: 'heros', component: Hero,props:true 
    }
  ]
})
